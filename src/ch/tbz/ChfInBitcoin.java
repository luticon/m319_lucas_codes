package ch.tbz;

import static java.lang.System.*;
import static java.lang.Math.*;
import java.util.*;

import static ch.tbz.lib.Input.*;

public class ChfInBitcoin {
    public static void main(String[] args ) {
        System.out.println("Conversion from CHF to Bitcoin:");
        out.println();
        double amountCHF = inputInt("Type in the amount of CHF you want to convert into Bitcoin: ");
        double BitcoinConversion = 0.000018;

        double Result = amountCHF * BitcoinConversion;
        out.println(amountCHF + " %f CHF is equivalent to %f in Bitcoin");
        out.printf("Der Amount %f CHF entspricht %f BTC", amountCHF, Result);
    }
}


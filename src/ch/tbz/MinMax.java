package ch.tbz;

import static ch.tbz.lib.Input.*;
import static java.lang.System.*;
import java.util.Objects;

public class MinMax {
    public static void main(String [] args ) {

        int[] numbers = new int[3];
        out.println("Welcome to MinMax! Please type in three numbers!");

        boolean allEqual;

        do {
            numbers[0] = inputInt("Type the first number:");
            numbers[1] = inputInt("Now the second: ");
            numbers[2] = inputInt("And lastly the third one:");

            allEqual = true;
            for (int i = 1; i < numbers.length; i++) {
                if (numbers[i] != numbers[0]) {
                    allEqual = false;
                    break;
                }
            }

            int largest = 0;
            int smallest = 0;

            if (!allEqual) {

                largest = numbers[0];
                smallest = numbers[0];

                for (int i = 1; i < numbers.length; i++) {

                    if (numbers[i] > largest) {
                        largest = numbers[i];
                    }
                    if (numbers[i] < smallest) {
                        smallest = numbers[i];
                    }
                }

                out.println("Smallest number: " + smallest);
                out.println("Largest number: " + largest);
                out.println("In order to close the program, type 3 equal numbers");

            }

        } while (!allEqual);

        out.println("All numbers are equal!");

    }
}

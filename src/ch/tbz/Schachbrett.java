package ch.tbz;

public class Schachbrett {

    public static void main (String [] args) {

        char [][] field = new char[8][8];
        char[] blackTopPieces = {'T', 'S', 'L', 'Q', 'K', 'L', 'S', 'T'};
        char [] blackBottomPieces = {'B','B','B','B','B','B','B','B'};

        char[] firstBlankRow = {' ', '.', ' ', '.', ' ', '.', ' ', '.'};
        char[] secondBlankRow = {'.', ' ', '.', ' ', '.', ' ', '.', ' '};


        for (int i = 0; i < 8; i++) {
            field[0][i] = blackTopPieces[i];
        }

        for (int i = 0; i < 8; i++) {
            field[1][i] = blackBottomPieces[i];
        }

        for (int i = 0; i < 8; i++) {
            field[2][i] = firstBlankRow[i];
            field[4][i] = firstBlankRow[i];
        }

        for (int i = 0; i < 8; i++) {
            field[3][i] = secondBlankRow[i];
            field[5][i] = secondBlankRow[i];
        }

        for (int i = 0; i < 8; i++) {
            field[6][i] = Character.toLowerCase(blackBottomPieces[i]);
        }

        for (int i = 0; i < 8; i++) {
            field[7][i] = Character.toLowerCase(blackTopPieces[i]);
        }

        for (int i = 0; i < 8; i++) {
            System.out.println("| " + field[i][0] + " | " + field[i][1] + " | " + field[i][2] + " | " + field[i][3]
            + " | " + field[i][4] + " | " + field[i][5] + " | " + field[i][6] + " | " + field[i][7] + " |");
        }

    }

}

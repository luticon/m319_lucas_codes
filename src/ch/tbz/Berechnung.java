package ch.tbz;

public class Berechnung {
    public static void main(String[] args )  {
        int gallons;
        double litres;

        for (gallons = 1; gallons <= 100; gallons ++) {
            if (gallons % 10 == 0) {
             System.out.println(" ");
            }
            litres = gallons / 3.7854;
            System.out.println(gallons + " gallons is " + litres + " litres.");
        }


    }
}
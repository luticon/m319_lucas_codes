package ch.tbz;

public class StringSort {
    public static void main(String [] args ) {
        String str = "Dieserstringwirdsortiert";

        String sortedString = sortString(str);

        System.out.println("Sorted String: " + sortedString);
    }


    public static String sortString(String input) {
        char[] arr = input.toCharArray();

        int i = 0;
        while (i < arr.length) {
            innerLoop(arr,i);
            i+=1;
        }

        return new String(arr);
    }

    public static void innerLoop (char[] arr, int i) {
        int j = i + 1;
        while (j < arr.length) {
            if (arr[j] < arr[i]) {
                swap(arr, i, j);
            }
            j+=1;
        }
    }

    public static void swap(char[] arr, int i, int j) {
        char temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}

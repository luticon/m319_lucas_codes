package ch.tbz;

import static ch.tbz.lib.Input.*;

public class NameAdresse {
    public static void main(String[] args ) {
        System.out.println("Geben Sie bitte Ihre Name und Adresse ein!");

        String Name = inputString("Bitte zuerst ihre Name: ");
        String Adresse = inputString("Jetzt bitte ihre Adresse");

        System.out.println("Name: " + Name);
        System.out.println("Adresse: " + Adresse);

    }
}

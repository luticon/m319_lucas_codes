package ch.tbz;

import static ch.tbz.lib.Input.*;
import static java.lang.System.*;
import static java.lang.Math.*;
import java.util.Objects;

public class KN_I1_SerieB_Tinoco {

    public static void main(String[] args) {

        out.println("GEOMETRISCHE BERECHNUNGEN");

        String entscheidung = "N";

        do {
            String Berechnung;

            do {
                Berechnung = inputString("Wollen Sie den Umfang/Fläche, das Volumen oder die Mantelfläche" +
                        " berechnen? Gültige Optionen: [Umfang + Fläche] [Volumen] [Mantelfläche]");

                if (!Berechnung.equals("Umfang + Fläche") &&
                        !Berechnung.equals("Volumen") &&
                        !Berechnung.equals("Mantelfläche")) {
                    out.println("Ungültige Eingabe. Bitte wählen Sie eine der gültigen Optionen");
                }

            } while (!Berechnung.equals("Umfang + Fläche") &&
                    !Berechnung.equals("Volumen") &&
                    !Berechnung.equals("Mantelfläche"));

            if (Berechnung.equals("Umfang + Fläche")) {

                String Form;

                do {
                    Form = inputString("Wählen Sie bitte eine Form. Gültige Optionen: (Rechteck, Dreieck, Kreis)");

                    if (!Form.equals("Rechteck") &&
                            !Form.equals("Dreieck") &&
                            !Form.equals("Kreis")) {
                        out.println("Ungültige Eingabe. Bitte wählen Sie eine der gültigen Optionen");
                    }

                } while (!Form.equals("Rechteck") &&
                        !Form.equals("Dreieck") &&
                        !Form.equals("Kreis"));

                if (Objects.equals(Form, "Rechteck")) {

                    double laenge = inputDouble("Länge in mm : ");
                    double breite = inputDouble("Breite in mm : ");

                    double umfang = (laenge * 2) + (breite * 2);
                    double flaeche = laenge * breite;

                    out.println("Der Umfang ist: " + umfang + " mm");
                    out.println("Die Fläche ist: " + flaeche + " mm");

                    do {
                        entscheidung = inputString("Eine weitere Berechnung? [Y]/[N] ");

                        if ((!entscheidung.equals("Y") &&
                                !entscheidung.equals("N"))) {
                            out.println("Ungültige Eingabe. Bitte wählen Sie eine der gültigen Optionen");
                        }

                    } while (!entscheidung.equals("Y") &&
                            !entscheidung.equals("N"));

                } else if (Form.equals("Dreieck")) {

                    double a = inputDouble("Geben Sie a in mm ein:");
                    double b = inputDouble("Geben Sie b in mm ein:");
                    double c = inputDouble("Geben Sie c in mm ein:");
                    String grundseite;
                    do {
                        grundseite = inputString("Geben Sie Ihre Grundseite ein. [a] [b] [c] ");
                    } while (!grundseite.equals("a") &&
                    !grundseite.equals("b") &&
                    !grundseite.equals("c"));

                    double g;
                    if (grundseite.equals("a")) {
                        g = a;
                    } else if (grundseite.equals("b")) {
                        g = b;
                    } else {
                        g = c;
                    }


                    double h = inputDouble("Geben Sie die Höhe in mm ein");

                    double umfang = a + b + c;
                    double flaeche = g * h / 2;

                    out.println("Umfang in mm: " + umfang);
                    out.println("Fläche in mm: " + flaeche);

                    do {
                        entscheidung = inputString("Eine weitere Berechnung? [Y]/[N] ");

                        if ((!entscheidung.equals("Y") &&
                                !entscheidung.equals("N"))) {
                            out.println("Ungültige Eingabe. Bitte wählen Sie eine der gültigen Optionen");
                        }

                    } while (!entscheidung.equals("Y") &&
                            !entscheidung.equals("N"));
                } else {

                    double r = inputDouble("Geben Sie bitte den Radius des Kreises in mm.");


                    double umfang = r * 2 * PI;
                    double flaeche = PI * pow(r,2);

                    out.println("Umfang in mm: " + umfang);
                    out.println("Fläche in mm: " + flaeche);

                    do {
                        entscheidung = inputString("Eine weitere Berechnung? [Y]/[N] ");

                        if ((!entscheidung.equals("Y") &&
                                !entscheidung.equals("N"))) {
                            out.println("Ungültige Eingabe. Bitte wählen Sie eine der gültigen Optionen");
                        }

                    } while (!entscheidung.equals("Y") &&
                            !entscheidung.equals("N"));

                }
            } else if (Berechnung.equals("Volumen")) {
                String Form;

                do {
                    Form = inputString("Wählen Sie bitte eine Form. Gültige Optionen: (Würfel, Zylinder, Kegel)");

                    if (!Form.equals("Würfel") &&
                            !Form.equals("Zylinder") &&
                            !Form.equals("Kegel")) {
                        out.println("Ungültige Eingabe. Bitte wählen Sie eine der gültigen Optionen");
                    }

                } while (!Form.equals("Würfel") &&
                        !Form.equals("Zylinder") &&
                        !Form.equals("Kegel"));

                if (Objects.equals(Form, "Würfel")) {

                    double a = inputDouble("Geben sie die Länge von der Seite a in mm:");

                    double volume = pow(a,3);

                    out.println("Das Volumen ist: " + volume + " mm");


                    do {
                        entscheidung = inputString("Eine weitere Berechnung? [Y]/[N] ");

                        if ((!entscheidung.equals("Y") &&
                                !entscheidung.equals("N"))) {
                            out.println("Ungültige Eingabe. Bitte wählen Sie eine der gültigen Optionen");
                        }

                    } while (!entscheidung.equals("Y") &&
                            !entscheidung.equals("N"));

                } else if (Form.equals("Zylinder")) {

                    double r = inputDouble("Geben Sie bitte den Radius des Zylinders in mm ein:");
                    double h = inputDouble("Geben Sie bitte die Höhe des Zylinders in mm ein:");

                    double volume = (pow(r,2) * PI) * h;

                    out.println("Das Volumen ist: " + volume + " mm");

                    do {
                        entscheidung = inputString("Eine weitere Berechnung? [Y]/[N] ");

                        if ((!entscheidung.equals("Y") &&
                                !entscheidung.equals("N"))) {
                            out.println("Ungültige Eingabe. Bitte wählen Sie eine der gültigen Optionen");
                        }

                    } while (!entscheidung.equals("Y") &&
                            !entscheidung.equals("N"));
                } else {

                    double r = inputDouble("Geben Sie bitte den Radius des Kegels in mm ein:");
                    double h = inputDouble("Geben Sie bitte die Höhe des Kegels in mm ein:");

                    double volume = (double) 1/3 * PI * pow(r,2) * h;

                    out.println("Das Volumen ist: " + volume + " mm");

                    do {
                        entscheidung = inputString("Eine weitere Berechnung? [Y]/[N] ");

                        if ((!entscheidung.equals("Y") &&
                                !entscheidung.equals("N"))) {
                            out.println("Ungültige Eingabe. Bitte wählen Sie eine der gültigen Optionen");
                        }

                    } while (!entscheidung.equals("Y") &&
                            !entscheidung.equals("N"));

                }
            }

            else if (Berechnung.equals("Mantelfläche")) {
                String Form;

                do {
                    Form = inputString("Wählen Sie bitte eine Form. Gültige Optionen: (Würfel, Zylinder, Kegel)");

                    if (!Form.equals("Würfel") &&
                            !Form.equals("Zylinder") &&
                            !Form.equals("Kegel")) {
                        out.println("Ungültige Eingabe. Bitte wählen Sie eine der gültigen Optionen");
                    }

                } while (!Form.equals("Würfel") &&
                        !Form.equals("Zylinder") &&
                        !Form.equals("Kegel"));

                if (Objects.equals(Form, "Würfel")) {

                    double a = inputDouble("Geben sie die Länge von der Seite a in mm:");

                    double mantelflaeche = pow(a,2) * 4;

                    out.println("Das Volumen ist: " + mantelflaeche + " mm");


                    do {
                        entscheidung = inputString("Eine weitere Berechnung? [Y]/[N] ");

                        if ((!entscheidung.equals("Y") &&
                                !entscheidung.equals("N"))) {
                            out.println("Ungültige Eingabe. Bitte wählen Sie eine der gültigen Optionen");
                        }

                    } while (!entscheidung.equals("Y") &&
                            !entscheidung.equals("N"));

                } else if (Form.equals("Zylinder")) {

                    double r = inputDouble("Geben Sie bitte den Radius des Zylinders r in mm ein:");
                    double h = inputDouble("Geben Sie bitte die Höhe des Zylinders h in mm ein:");

                    double mantelflaeche = 2 * PI * r * h;

                    out.println("Das Volumen ist: " + mantelflaeche + " mm");

                    do {
                        entscheidung = inputString("Eine weitere Berechnung? [Y]/[N] ");

                        if ((!entscheidung.equals("Y") &&
                                !entscheidung.equals("N"))) {
                            out.println("Ungültige Eingabe. Bitte wählen Sie eine der gültigen Optionen");
                        }

                    } while (!entscheidung.equals("Y") &&
                            !entscheidung.equals("N"));
                } else {

                    double r = inputDouble("Geben Sie bitte den Radius r des Kegels in mm ein:");
                    double s = inputDouble("Geben Sie bitte die Mantellinie s des Kegels in mm ein:");

                    double mantelflaeche = PI * r * s;

                    out.println("Das Volumen ist: " + mantelflaeche + " mm");

                    do {
                        entscheidung = inputString("Eine weitere Berechnung? [Y]/[N] ");

                        if ((!entscheidung.equals("Y") &&
                                !entscheidung.equals("N"))) {
                            out.println("Ungültige Eingabe. Bitte wählen Sie eine der gültigen Optionen");
                        }

                    } while (!entscheidung.equals("Y") &&
                            !entscheidung.equals("N"));

                }
            }

            else {
                entscheidung = "N";
            }

        } while (entscheidung.equals("Y"));

        out.println("Thank You for using me");
    }

}
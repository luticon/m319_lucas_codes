package ch.tbz;

public class TestWhile {

    public static void main(String [] args) {

        int x = 0;

        while (x < 10) {
            x = x + 1;
            System.out.println("The current number is " + x);
        }

    }
}
